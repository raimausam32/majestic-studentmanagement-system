<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        include 'db_conn.php';

        $errors = array();
        $data = [];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $dob = $_POST['dob'];
        $enrollment_date = $_POST['enrollment_date'];
        $school_year = $_POST['school_year'];

        $home_phone = $_POST['home_phone'];
        $mobile_phone = $_POST['mobile_phone'];
        $email = $_POST['email'];


        $contact_name = $_POST['contact_name'];
        $contact_phone = $_POST['contact_phone'];
        $second_contact_name = $_POST['second_contact_name'];
        $second_contact_phone = $_POST['second_contact_phone'];
        $data = [
            $first_name, $last_name, $dob, $enrollment_date, $school_year, $home_phone, $mobile_phone, $email, $contact_name, $contact_phone, $second_contact_name, $second_contact_phone
        ];
        try {
            $sql = "Insert into students (first_name,last_name,dob,enrollment_date,school_year,home_phone,mobile_phone,email,contact_name,contact_phone,second_contact_name,second_contact_phone)values
                    ('$first_name', '$last_name', '$dob', '$enrollment_date', '$school_year', '$home_phone', '$mobile_phone', '$email', '$contact_name', '$contact_phone', '$second_contact_name', '$second_contact_phone')";
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
                $_SESSION['success_message'] = "Student Added Successfully!";
                header('Location: home.php');
                exit();
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            $conn->close();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die('here');
        }
    }


?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body>
        <nav class="navbar navbar-expand-sm bg-dark navbar-dark">
            <!-- Brand -->
            <a class="navbar-brand" href="#">Majestic</a>

            <!-- Links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" href="home.php">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="logout.php">Logout</a>
                </li>

                <!-- <li class="nav-item dropdown">
                         <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
                              Dropdown link
                         </a>
                         <div class="dropdown-menu">
                              <a class="dropdown-item" href="#">Link 1</a>
                              <a class="dropdown-item" href="#">Link 2</a>
                              <a class="dropdown-item" href="#">Link 3</a>
                         </div>
                    </li> -->
            </ul>
        </nav>
        <br>

        <div class="container">
            <h2>Fill Student Details</h2>

            <form action="student_new.php" method="post">
                <div class="row">
                    <div class="col">
                        <input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name" required>
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" required>
                    </div>

                </div>
                <br>
                <div class="form-row">
                    <div class="col">
                        <label for="uname">Dob:</label>
                        <input type="date" class="form-control" placeholder="Enter Date Of Birth" name="dob" required>
                    </div>
                    <div class="col">
                        <label for="uname">Enrollment Date:</label>
                        <input type="date" class="form-control" placeholder="Enter Enrollment Date" name="enrollment_date">
                    </div>
                    <div class="col">
                        <label for="uname">School Year:</label>
                        <input type="date" class="form-control" placeholder="School Year" name="school_year" required>
                    </div>
                </div>

                <br>
                <div class="form-row">
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Home Phone" name="home_phone">
                    </div>
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Mobile Phone" name="mobile_phone" required>
                    </div>
                    <div class="col">

                        <input type="email" class="form-control" placeholder="Enter Email" name="email" required>
                    </div>
                </div>

                <br>
                <div class="form-row">
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter First Contact Name" name="contact_name">
                    </div>
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter First Contact Phone" name="contact_phone">
                    </div>

                </div>

                <br>
                <div class="form-row">
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Second Contact Name" name="second_contact_name">
                    </div>
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Second Contact Phone" name="second_contact_phone">
                    </div>

                </div>
                <br>

                <button type="submit" class="btn btn-primary">Submit</button>

            </form>
        </div>


        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

    </html>
<?php
} else {
    header("Location: index.php");
    exit();
}
?>