<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {



?>
     <!DOCTYPE html>
     <html lang="en">

     <head>
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     </head>

     <body>
          <?php
          include('common/header.php');
          ?>

          <div class="container">
               <h2>Welcome <?php echo $_SESSION['user_name']; ?></h2>

               <p>Student Reports</p>

               <table class="table">
                    <thead>
                         <tr>

                              <th>School Year</th>
                              <th>Total Student</th>

                         </tr>
                    </thead>
                    <tbody>
                         <?php
                         include 'db_conn.php';
                         $sql = "SELECT count(id) as total_count,year(school_year) as school_year FROM majestic.students
                         group by year(school_year)";
                         $reports = [];
                         $result = $conn->query($sql);
                         if ($result->num_rows > 0) {
                              while ($row = $result->fetch_assoc()) {

                         ?>
                                   <tr>

                                        <td><?php echo $row['school_year'] ?></td>
                                        <td><?php echo $row['total_count'] ?></td>

                                   </tr>
                         <?php
                              }
                         } else {
                              echo "Error: " . $sql . "<br>" . $conn->error;
                         }
                         $conn->close();
                         ?>
                    </tbody>
               </table>
          </div>


     </body>

     </html>
<?php
} else {
     header("Location: index.php");
     exit();
}
?>