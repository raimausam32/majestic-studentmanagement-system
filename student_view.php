<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {
    include 'db_conn.php';
    if ($_SERVER['REQUEST_METHOD'] == 'GET') {
        $studentId = $_GET['student_id'];
        try {
            $sql = "select * from students where id='$studentId'";
            $studentdetail = [];
            $result = $conn->query($sql);
            if ($result->num_rows > 0) {
                while ($row = $result->fetch_assoc()) {
                    $studentdetail = $row;
                    // var_dump($row);
                }
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            $conn->close();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die('here');
        }
    }

    if ($_SERVER['REQUEST_METHOD'] == 'POST') {

        $errors = array();
        $data = [];
        $student_id = $_POST['student_id'];
        $first_name = $_POST['first_name'];
        $last_name = $_POST['last_name'];
        $dob = $_POST['dob'];
        $enrollment_date = $_POST['enrollment_date'];
        $school_year = $_POST['school_year'];

        $home_phone = $_POST['home_phone'];
        $mobile_phone = $_POST['mobile_phone'];
        $email = $_POST['email'];


        $contact_name = $_POST['contact_name'];
        $contact_phone = $_POST['contact_phone'];
        $second_contact_name = $_POST['second_contact_name'];
        $second_contact_phone = $_POST['second_contact_phone'];
        $data = [
            $first_name, $last_name, $dob, $enrollment_date, $school_year, $home_phone, $mobile_phone, $email, $contact_name, $contact_phone, $second_contact_name, $second_contact_phone
        ];
        try {
            $sql = "UPDATE students set first_name = '$first_name',last_name='$last_name',dob='$dob', enrollment_date='$enrollment_date', school_year='$school_year', home_phone='$home_phone', mobile_phone='$mobile_phone',
             email='$email', contact_name='$contact_name', contact_phone='$contact_phone', second_contact_name='$second_contact_name', second_contact_phone='$second_contact_phone' where id=$student_id";
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
                $_SESSION['success_message'] = "Student Added Successfully!";
                header('Location: students.php');
                exit();
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
            $conn->close();
        } catch (\Exception $e) {
            var_dump($e->getMessage());
            die('here');
        }
    }

?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    </head>

    <body>
        <?php
        include('common/header.php');
        ?>

        <div class="container">
            <h2>Fill Student Details</h2>

            <form action="student_view.php" method="post">
                <input type="hidden" name="student_id" value="<?php echo (isset($studentdetail) ? $studentdetail['id'] : ''); ?>" />
                <div class="row">
                    <div class="col">
                        <input type="text" class="form-control" id="first_name" placeholder="Enter First Name" name="first_name" required value="<?php echo (isset($studentdetail) ? $studentdetail['first_name'] : ''); ?>">
                    </div>
                    <div class="col">
                        <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" required value="<?php echo (isset($studentdetail) ? $studentdetail['last_name'] : ''); ?>">
                    </div>

                </div>
                <br>
                <div class="form-row">
                    <div class="col">
                        <label for="uname">Dob:</label>
                        <input type="date" class="form-control" placeholder="Enter Date Of Birth" name="dob" required value="<?php echo (isset($studentdetail) ? $studentdetail['dob'] : ''); ?>">
                    </div>
                    <div class="col">
                        <label for="uname">Enrollment Date:</label>
                        <input type="date" class="form-control" placeholder="Enter Enrollment Date" name="enrollment_date" value="<?php echo (isset($studentdetail) ? $studentdetail['enrollment_date'] : ''); ?>">
                    </div>
                    <div class="col">
                        <label for="uname">School Year:</label>
                        <input type="date" class="form-control" placeholder="School Year" name="school_year" required value="<?php echo (isset($studentdetail) ? $studentdetail['school_year'] : ''); ?>">
                    </div>
                </div>

                <br>
                <div class="form-row">
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Home Phone" name="home_phone" value="<?php echo (isset($studentdetail) ? $studentdetail['home_phone'] : ''); ?>">
                    </div>
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Mobile Phone" name="mobile_phone" required value="<?php echo (isset($studentdetail) ? $studentdetail['mobile_phone'] : ''); ?>">
                    </div>
                    <div class="col">

                        <input type="email" class="form-control" placeholder="Enter Email" name="email" required value="<?php echo (isset($studentdetail) ? $studentdetail['email'] : ''); ?>">
                    </div>
                </div>

                <br>
                <div class="form-row">
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter First Contact Name" name="contact_name" value="<?php echo (isset($studentdetail) ? $studentdetail['contact_name'] : ''); ?>">
                    </div>
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter First Contact Phone" name="contact_phone" value="<?php echo (isset($studentdetail) ? $studentdetail['contact_phone'] : ''); ?>">
                    </div>

                </div>

                <br>
                <div class="form-row">
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Second Contact Name" name="second_contact_name" value="<?php echo (isset($studentdetail) ? $studentdetail['second_contact_name'] : ''); ?>">
                    </div>
                    <div class="col">

                        <input type="text" class="form-control" placeholder="Enter Second Contact Phone" name="second_contact_phone" value="<?php echo (isset($studentdetail) ? $studentdetail['second_contact_phone'] : ''); ?>">
                    </div>

                </div>
                <br>
                <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal"> Delete</button>
                <button type="submit" name="update" class="btn btn-primary"> Update</button>

            </form>
        </div>
        <div class="modal fade" id="myModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                    <form action="student_del.php" method="post">
                        <input type="hidden" name="student_id" value="<?php echo $studentdetail['id']; ?>" />
                        <!-- Modal Header -->
                        <div class="modal-header">
                            <h4 class="modal-title">Delete Confirmation</h4>
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>

                        <!-- Modal body -->
                        <div class="modal-body">
                            Are you sure you want to delete student detail ?
                        </div>

                        <!-- Modal footer -->
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success" id="deleteConfirmation">Delete</button>
                        </div>

                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>

    </html>
<?php
} else {
    header("Location: index.php");
    exit();
}
?>