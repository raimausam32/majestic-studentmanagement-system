<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {

?>
     <!DOCTYPE html>
     <html lang="en">

     <head>
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     </head>

     <body>
          <?php
          include('common/header.php');
          ?>

          <div class="container">
               <h2>Students</h2>
               <a href="student_create.php" class="btn btn-primary">New Students</a>

               <table class="table">
                    <thead>
                         <tr>
                              <th>Student Name</th>
                              <th>School Year</th>
                              <th>Email</th>
                              <th>Operation</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php
                         include 'db_conn.php';
                         $sql =  mysqli_query($conn, "SELECT * FROM students order by school_year ASC,first_name ASC");
                         while ($row = mysqli_fetch_assoc($sql)) {
                              $sid = $row['id'];
                         ?>
                              <tr>
                                   <td><?php echo $row['first_name'] . '' . $row['last_name'] ?></td>
                                   <td><?php echo $row['school_year'] ?></td>
                                   <td><a href="mailto:<?php echo $row['email']; ?>"><?php echo $row['email'] ?></a></td>
                                   <td><a href="student_view.php?student_id=<?php echo $row['id']; ?>" title="View Student Details">View</a> |
                                        <a href="" title="Delete Student" data-studentid="<?php echo $row['id']; ?>" class="studentdelete">Delete</a>
                                   </td>
                              </tr>
                         <?php
                         }
                         mysqli_close($conn);
                         ?>
                    </tbody>
               </table>
          </div>

          <div class="modal fade" id="myModal">
               <div class="modal-dialog modal-sm">
                    <div class="modal-content">
                         <form action="student_del.php" method="post">
                              <input type="hidden" name="student_id" value="" />
                              <!-- Modal Header -->
                              <div class="modal-header">
                                   <h4 class="modal-title">Delete Confirmation</h4>
                                   <button type="button" class="close" data-dismiss="modal">&times;</button>
                              </div>

                              <!-- Modal body -->
                              <div class="modal-body">
                                   Are you sure you want to delete student detail ?
                              </div>

                              <!-- Modal footer -->
                              <div class="modal-footer">
                                   <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                   <button type="submit" class="btn btn-success" id="deleteConfirmation">Delete</button>
                              </div>

                    </div>
               </div>
          </div>
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
          <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
          </script>
          <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
     </body>
     <script>
          $(document).ready(function() {
               $(document).on('click', '.studentdelete', function(e) {
                    e.preventDefault();
                    const studentId = $(this).data('studentid');
                    $('input[name="student_id"]').val(studentId);
                    $('#myModal').modal('show');
               });


          });
     </script>

     </html>
<?php
} else {
     header("Location: index.php");
     exit();
}
?>