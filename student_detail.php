<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {

?>
     <!DOCTYPE html>
     <html lang="en">

     <head>
          <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
     </head>

     <body>
          <?php
          include('common/header.php');
          ?>

          <div class="container">
               <h2>Students</h2>
               <a href="student_new.php" class="btn btn-primary">New Students</a>

               <table class="table">
                    <thead>
                         <tr>
                              <th>Student Name</th>
                              <th>Email</th>
                              <th>Contact Detail</th>
                              <th>Operation</th>
                         </tr>
                    </thead>
                    <tbody>
                         <?php
                         include 'db_conn.php';
                         $sql =  mysqli_query($conn, "SELECT * FROM students order by id desc ");
                         while ($row = mysqli_fetch_assoc($sql)) {
                              $sid = $row['id'];
                         ?>
                              <tr>
                                   <td><?php echo $row['first_name'] . '' . $row['last_name'] ?></td>
                                   <td><?php echo $row['email'] ?></td>
                                   <td><?php echo $row['mobile_number'] ?></td>
                                   <td><a href="" title="View Student Details">View</a><a href="" title="Delete Student">Delete</a></td>
                              </tr>
                         <?php
                         }
                         mysqli_close($conn);
                         ?>
                    </tbody>
               </table>
          </div>

          <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
          <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
          <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
     </body>

     </html>
<?php
} else {
     header("Location: index.php");
     exit();
}
?>