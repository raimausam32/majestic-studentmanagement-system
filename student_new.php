<?php
session_start();

if (isset($_SESSION['id']) && isset($_SESSION['user_name'])) {

     if ($_SERVER['REQUEST_METHOD'] == 'POST') {

          include 'db_conn.php';

          $errors = array();
          $data = [];
          $first_name = $_POST['first_name'];
          $last_name = $_POST['last_name'];
          $dob = $_POST['dob'];
          $enrollment_date = $_POST['enrollment_date'];
          $school_year = $_POST['school_year'];

          $home_phone = $_POST['home_phone'];
          $mobile_phone = $_POST['mobile_phone'];
          $email = $_POST['email'];


          $contact_name = $_POST['contact_name'];
          $contact_phone = $_POST['contact_phone'];
          $second_contact_name = $_POST['second_contact_name'];
          $second_contact_phone = $_POST['second_contact_phone'];
          $data = [
               $first_name, $last_name, $dob, $enrollment_date, $school_year, $home_phone, $mobile_phone, $email, $contact_name, $contact_phone, $second_contact_name, $second_contact_phone
          ];
          try {
               $sql = "Insert into students (first_name,last_name,dob,enrollment_date,school_year,home_phone,mobile_phone,email,contact_name,contact_phone,second_contact_name,second_contact_phone)values
                    ('$first_name', '$last_name', '$dob', '$enrollment_date', '$school_year', '$home_phone', '$mobile_phone', '$email', '$contact_name', '$contact_phone', '$second_contact_name', '$second_contact_phone')";
               if ($conn->query($sql) === TRUE) {
                    echo "New record created successfully";
                    $_SESSION['success_message'] = "Student Added Successfully!";
                    header('Location: home.php');
                    exit();
               } else {
                    echo "Error: " . $sql . "<br>" . $conn->error;
               }
               $conn->close();
          } catch (\Exception $e) {
               var_dump($e->getMessage());
               die('here');
          }
     }

?>
     <!DOCTYPE html>
     <html>

     <head>
          <title>HOME</title>
          <link rel="stylesheet" type="text/css" href="style.css">
     </head>

     <body>
          <h1>Hello, <?php echo $_SESSION['name']; ?></h1>
          <a href="logout.php">Logout</a>

          <h1 class="page-header">STUDENTS <button style="float:right" type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">


                    <div class="modal fade" id="myModal" role="dialog">
                         <div class="modal-dialog modal-lg">


                              <div class="modal-content">
                                   <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">New Students</h4>
                                   </div>
                                   <div class="modal-body">




                                        <form class="form-horizontal" action="student_new.php" method="post">
                                             <fieldset>
                                                  <div class="container">

                                                       <div class="col-md-12" style="width:70%;border-bottom:1px solid #333">
                                                            <h4><b>Student's Personal Details </b></h4>
                                                       </div>
                                                       <br>
                                                       <br>
                                                       <div class="col-md-4">


                                                            <div class="form-group">
                                                                 <label class="col-xs-4 control-label" for="name">Name</label>
                                                                 <div class="col-xs-8">
                                                                      <div class="input-group">
                                                                           <input id="name" class="form-control input-xs" style="text-transform: capitalize;" name="first_name" placeholder="First Name" type="text">
                                                                           <input id="name" class="form-control input-xs" style="text-transform: capitalize;" name="last_name" placeholder="Last Name" type="text">

                                                                      </div>
                                                                 </div>
                                                            </div>

                                                       </div>


                                                       <div class="col-md-4">
                                                            <br>

                                                            <div class="form-group">
                                                                 <label class="col-xs-5 control-label" for="dob">Date of Birth</label>
                                                                 <div class="col-xs-7">
                                                                      <input id="dob" name="dob" type="date" placeholder="YYYY-MM-DD" class="form-control input-md">
                                                                 </div>
                                                            </div>
                                                            <br>

                                                            <div class="form-group">
                                                                 <label class="col-xs-5 control-label" for="enrollment_date">Enrollment Date</label>
                                                                 <div class="col-xs-7">
                                                                      <input id="enrollment_date" name="enrollment_date" type="date" placeholder="YYYY-MM-DD" class="form-control input-md">
                                                                 </div>
                                                            </div>
                                                            <br>
                                                            <div class="form-group">
                                                                 <label class="col-xs-2 control-label" for="sy">School Year</label>

                                                                 <div class="col-xs-10">
                                                                      <input class="form-control" style="width:150px" id="sy" name="sy" type="text" placeholder="FROM-TO">

                                                                 </div>
                                                            </div>

                                                            <br>


                                                            <div class="form-group">
                                                                 <label class="col-xs-5 control-label" for="pg">Contact Details</label>
                                                                 <div class="col-xs-7">
                                                                      <div class="input-group">
                                                                           <input id="pg" name="home_phone" class="form-control" style="text-transform: capitalize;" placeholder="Enter Home Phone" type="text">
                                                                           <input id="pg" name="mobile_php" class="form-control" style="text-transform: capitalize;" placeholder="Enter Mobile Phone" type="text">
                                                                           <input id="email" name="email" class="form-control" style="text-transform: capitalize;" placeholder="Enter Email" type="text">

                                                                      </div>
                                                                 </div>
                                                            </div>
                                                            <br>
                                                            <div class="form-group">
                                                                 <label class="col-xs-5 control-label" for="pg">1st Contact Details</label>
                                                                 <div class="col-xs-7">
                                                                      <div class="input-group">
                                                                           <input id="contact_phone" name="contact_phone" class="form-control" style="text-transform: capitalize;" placeholder="Enter 1st Contact Phone" type="text">
                                                                           <input id="contact_address" name="contact_address" class="form-control" style="text-transform: capitalize;" placeholder="Enter 1st Contact Address" type="text">


                                                                      </div>
                                                                 </div>
                                                            </div>
                                                            <br>
                                                            <div class="form-group">
                                                                 <label class="col-xs-5 control-label" for="pg">2nd Contact Details</label>
                                                                 <div class="col-xs-7">
                                                                      <div class="input-group">
                                                                           <input id="second_contact_phone" name="second_contact_phone" class="form-control" style="text-transform: capitalize;" placeholder="Enter 2nd Contact Phone" type="text">
                                                                           <input id="second_contact_address" name="second_contact_address" class="form-control" style="text-transform: capitalize;" placeholder="Enter 2nd Contact Address" type="text">

                                                                      </div>
                                                                 </div>
                                                            </div>
                                                       </div>
                                                  </div>
                                             </fieldset>





                                   </div>
                                   <div class="modal-footer">

                                        <input type="submit" class="btn btn-success " name="submit" value="Submit Form">

                                        </form>
                                        <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                                   </div>
                              </div>

                         </div>
                    </div>



     </body>

     </html>

<?php
} else {
     header("Location: index.php");
     exit();
}
?>